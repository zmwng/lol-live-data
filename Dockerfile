FROM golang:alpine AS build
RUN apk add --no-cache git ca-certificates
RUN go get github.com/golang/dep/cmd/dep
WORKDIR /go/src/lol-live-data/
COPY . .
RUN dep ensure -vendor-only
RUN GOOS=linux go build -o /bin/lol-live-data

FROM alpine
EXPOSE 5000
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /bin/lol-live-data /bin/lol-live-data
ENTRYPOINT ["/bin/lol-live-data"]